from django.http import JsonResponse
from .models import Technician, Appointment, AutomobileVO
from common.json import ModelEncoder
import json
from django.views.decorators.http import require_http_methods



class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = {
        "first_name",
        "last_name",
        "employee_id",
        "id"
    }

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = {
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
        "id"
    }
    encoders = {"technician": TechnicianListEncoder()}

class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = {
        "vin",
        "sold",
        "id"
    }

# List view for technician and create
@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder
        )
    elif request.method == "POST":
        content = json.loads(request.body)

        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianListEncoder,
            safe=False
        )

# Detail view for technicians and delete
@require_http_methods(["GET", "DELETE"])
def show_technician(request, id):
    if request.method == "GET":
        technician = Technician.objects.get(id=id)
        return JsonResponse(
            {"technician": technician},
            encoder=TechnicianListEncoder
        )
    elif request.method == "DELETE":
        count,_ = Technician.objects.filter(id=id).delete()
        return JsonResponse({'deleted': count > 0})


# List view for appointments and create
@require_http_methods(["GET", "POST", "PUT"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments},
            encoder=AppointmentListEncoder
        )
    elif request.method == "POST":
        content = json.loads(request.body)
        try:
            id = content["technician"]
            technician = Technician.objects.get(id=id)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid technician"}, status=400
            )
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False
        )


# Detail view for appointment and delete
@require_http_methods(["GET", "DELETE"])
def show_appointment(request, id):
    if request.method == "GET":
        appointment = Appointment.objects.get(id=id)
        return JsonResponse(
            {"appointment": appointment},
            encoder=AppointmentListEncoder
        )
    elif request.method == "DELETE":
        count,_ = Appointment.objects.filter(id=id).delete()
        return JsonResponse({'deleted': count > 0})

@require_http_methods(["PUT"])
def update_status(request, id):
    if request.method == "PUT":
        appointment = Appointment.objects.get(id=id)
        content = json.loads(request.body)
        new = content.get('status')
        if new in ['created', 'finished', 'canceled']:
            appointment.status = new
            appointment.save()
            return JsonResponse(
                {"message": f"Appointment {appointment.id} status has been updated."})
        else:
            return JsonResponse(
                {"message": "Appointment could not update."}
                , status=400)
