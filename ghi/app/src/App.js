import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import TechnicianList from "./TechnicianList";
import CreateTechnician from "./CreateTechnicianForm";
import AppointmentList from "./AppointmentList";
import CreateAppointment from "./CreateAppointmentForm";
import ServiceHistory from "./ServiceHistory";
import ListManufacturer from "./ManufacturerList";
import CreateManufacturer from "./CreateManufacturer";
import VehicleModelList from "./VehicleList";
import CreateVehicleModel from "./CreateVehicleForm";
import AutomobilesList from "./AutomobilesList";
import AutomobileForm from "./AutomobileForm";
import SalespeopleList from "./SalespeopleList";
import CreateSalesperson from "./CreateSalesperson";
import CustomerList from "./CustomerList";
import CreateCustomer from "./CreateCustomer";
import SalesList from "./SalesList";
import CreateSale from "./CreateSale";
import SalesHistory from "./SalesHistory";

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="models">
            <Route
              path=""
              element={<VehicleModelList models={props.models} />}
            />
            <Route path="new" element={<CreateVehicleModel />} />
          </Route>
          <Route path="manufacturers">
            <Route
              path=""
              element={<ListManufacturer manufacturers={props.manufacturers} />}
            />
            <Route path="new" element={<CreateManufacturer />} />
          </Route>
          <Route path="technicians">
            <Route
              path=""
              element={<TechnicianList technicians={props.technicians} />}
            />
            <Route path="new" element={<CreateTechnician />} />
          </Route>
          <Route path="appointments">
            <Route
              path=""
              element={<AppointmentList appointments={props.appointments} />}
            />
            <Route path="new" element={<CreateAppointment />} />
            <Route path="servicehistory" element={<ServiceHistory />} />
          </Route>
          <Route path="automobiles" element={<AutomobilesList automobiles={props.automobiles} />} />
          <Route path="automobiles/new" element={<AutomobileForm />} />
          <Route path="salespeople" element={<SalespeopleList salepeople={props.salespeople} />} />
          <Route path="salespeople/new" element={<CreateSalesperson />} />
          <Route path="customer" element={<CustomerList customer={props.customer} />} />
          <Route path="customer/new" element={<CreateCustomer />} />
          <Route path="sales" element={<SalesList sales={props.sales} />} /> 
          <Route path="sales/new" element={<CreateSale />} /> 
          <Route path="sales/history" element={<SalesHistory salespeople={props.salespeople} />} /> 
         
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
