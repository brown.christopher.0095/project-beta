import React, { useEffect, useState } from 'react';

function SalesList() {
    const [sales, setSales] = useState([]);

    const fetchData = async () => {
        try {
            const response = await fetch('http://localhost:8090/api/sales/');
            if (response.ok) { 
                const data = await response.json();
                setSales(data.sales); 
            } else {
                console.error("Failed to fetch Sales:", response.statusText);
            }
        } catch (e) {
            console.error("Error fetching Sales:", e);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const handleDelete = async (salesId) => {
        try {
            const response = await fetch(`http://localhost:8090/api/sales/${salesId}`, {
                method: 'DELETE',
            });
            if (response.ok) {
                fetchData(); 
            } else {
                console.error("Failed to delete sale:", response.statusText);
            }
        } catch (e) {
            console.error("Error deleting sale:", e);
        }
    };

    return (
        <div>
            <h2>Sales List</h2>
            <table className="table">
                <thead>
                    <tr>
                        <th>Price</th>
                        <th>Customer Name</th>
                        <th>Employee ID</th>
                        <th>Automobile VIN</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map((sale) => (
                        <tr key={sale.automobile.vin}>
                            <td>{sale.price}</td>
                            <td>{`${sale.customer.first_name} ${sale.customer.last_name}`}</td>
                            <td>{sale.sales_person ? sale.sales_person.employee_id : 'N/A'}</td>
                            <td>{sale.automobile.vin}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default SalesList;