import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-info">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <NavLink className="nav-link" to="/technicians">List Technicians</NavLink>
        <NavLink className="nav-link" to="/technicians/new">Create Technicians</NavLink>
        <NavLink className="nav-link" to="/appointments">List Appointment</NavLink>
        <NavLink className="nav-link" to="/appointments/new">Create Appointment</NavLink>
        <NavLink className="nav-link" to="/manufacturers">List Manufacturers</NavLink>
        <NavLink className="nav-link" to="/manufacturers/new">Create Manufacturer</NavLink>
        <NavLink className="nav-link" to="/models">List Models</NavLink>
        <NavLink className="nav-link" to="/models/new">Create Model</NavLink>
        <NavLink className="nav-link" to="/appointments/servicehistory">Service History</NavLink>
        <NavLink className="nav-link" to="/automobiles">List Automobiles</NavLink>
        <NavLink className="nav-link" to="/automobiles/new">Create Automobiles</NavLink>
        <NavLink className="nav-link" to="/salespeople">List Salespeople</NavLink>
        <NavLink className="nav-link" to="/salespeople/new">Create Salespeople</NavLink>
        <NavLink className="nav-link" to="/customer">List Customer</NavLink>
        <NavLink className="nav-link" to="/customer/new">Create Customer</NavLink>
        <NavLink className="nav-link" to="/sales">List All Sales</NavLink>
        <NavLink className="nav-link" to="/sales/new">Make Sale</NavLink> 
        <NavLink className="nav-link" to="/sales/history">Sales History</NavLink>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
