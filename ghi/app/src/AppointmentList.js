import React, { useEffect, useState } from "react";


function AppointmentList() {
  const [appointments, setAppointments] = useState([]);

  const fetchData = async () => {
    const response = await fetch("http://localhost:8080/api/appointments/");
    try {
      if (response.ok) {
        const data = await response.json();
        setAppointments(data.appointments);
        const filteredStatus = data.appointments.filter(
          appointment => appointment.status === "created"
        );
        setAppointments(filteredStatus)
      }
    } catch (e) {
      console.log("Error fetching appointments");
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleStatusChange = async (appointmentId, newStatus) => {
    const response = await fetch(`http://localhost:8080/api/appointments/${appointmentId}/`, {
      method: 'put',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ status: newStatus })
    });

    try {
      if (response.ok) {
        fetchData();
      }
    } catch (e) {
      console.error('Error updating the status:', e);
    }
  };

  const handleDelete = async (appointmentId) => {
    const response = await fetch(`http://localhost:8080/api/appointments/${appointmentId}/`, {
      method: 'DELETE'
    })
    try {
      if (response.ok) {
        fetchData();
      }
    } catch (e) {
      console.log('Error Deleting item', e)
    }
  };

  return (
    <div>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>VIN</th>
            <th>Customer</th>
            <th>Date/Time</th>
            <th>Technician</th>
            <th>Reason</th>
            <th>Status</th>
            <th>Change Status</th>
          </tr>
        </thead>
        <tbody>
          {appointments.map((appointment) => {
            return (
              <tr key={appointment.id}>
                <td>{appointment.vin}</td>
                <td>{appointment.customer}</td>
                <td>{appointment.date_time}</td>
                <td>{appointment.technician.first_name}</td>
                <td>{appointment.reason}</td>
                <td>{appointment.status}</td>
                <td>
                  <button className="btn btn-primary" onClick={() => handleStatusChange(appointment.id, 'finished')}>Finished</button>
                </td>
                <td>
                  <button className="btn btn-warning" onClick={() => handleStatusChange(appointment.id, 'canceled')}>Cancelled</button>
                </td>
                <td>
                  <button className="btn btn-danger" onClick={() => handleDelete(appointment.id)}>Delete</button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default AppointmentList;
