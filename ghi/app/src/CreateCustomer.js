import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';

function CreateCustomer() {
  const [customer, setCustomer] = useState({
    first_name: '',
    last_name: '',
    address: '',
    phone_number: '',
  });
  const navigate = useNavigate();

  const handleChange = (event) => {
    const { name, value } = event.target;
    setCustomer((prevState) => ({ ...prevState, [name]: value }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const response = await fetch('http://localhost:8090/api/customers/', {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(customer),
      });
      if (response.ok) {
        navigate('/customers');
      } else {
        const errorText = await response.text();
        throw new Error('Failed to create customer: ' + errorText);
      }
    } catch (error) {
      console.error("Error adding customer:", error.message);
      alert("Error adding customer: " + error.message);
    }
  };

  return (
    <div className="container my-4">
      <h2>Create Customer</h2>
      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label htmlFor="first_name" className="form-label">First Name:</label>
          <input type="text" id="first_name" name="first_name" className="form-control" value={customer.first_name} onChange={handleChange} required />
        </div>
        <div className="mb-3">
          <label htmlFor="last_name" className="form-label">Last Name:</label>
          <input type="text" id="last_name" name="last_name" className="form-control" value={customer.last_name} onChange={handleChange} required />
        </div>
        <div className="mb-3">
          <label htmlFor="address" className="form-label">Address:</label>
          <input type="text" id="address" name="address" className="form-control" value={customer.address} onChange={handleChange} required />
        </div>
        <div className="mb-3">
          <label htmlFor="phone_number" className="form-label">Phone Number:</label>
          <input type="text" id="phone_number" name="phone_number" className="form-control" value={customer.phone_number} onChange={handleChange} required />
        </div>
        <button type="submit" className="btn btn-primary">Create Customer</button>
      </form>
    </div>
  );
}

export default CreateCustomer;