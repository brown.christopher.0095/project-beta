import React, { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

function CreateVehicle() {
  const [formData, setFormData] = useState({
    name: "",
    picture_url: "",
    manufacturer_id: "",
    // manufacturer_id because in view function that is how content is grabbed for manufacturers
  });
  const [manufacturers, setManufacturers] = useState([]);
  const navigate = useNavigate();

  useEffect(() => {
    fetchManufacturers();
  }, []);

  const fetchManufacturers = async () => {
    try {
      const response = await fetch("http://localhost:8100/api/manufacturers/");
      if (response.ok) {
        const data = await response.json();
        setManufacturers(data.manufacturers);
      } else {
        console.error("Failure getting manufacturers");
      }
    } catch (e) {
      console.error("Error getting manufacturers:", e);
    }
  };

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData({ ...formData, [name]: value });
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const response = await fetch("http://localhost:8100/api/models/", {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(formData),
      });

      if (response.ok) {
        navigate("/models");
      } else {
        console.error("Failure creating vehicle model");
      }
    } catch (e) {
      console.error("Error while trying to create vehicle model:", e);
    }
  };

  return (
    <form className="row g-3" onSubmit={handleSubmit}>
      <div>
        <label>
          Model Name
          <input
            value={formData.name}
            onChange={handleChange}
            required
            className="form-control"
            type="text"
            id="name"
            name="name"
            placeholder="Name"
          />
        </label>
      </div>
      <div>
        <label>
          Picture URL
          <input
            className="form-control"
            type="url"
            value={formData.picture_url}
            name="picture_url"
            onChange={handleChange}
            required
            placeholder="Picture Url"
          />
        </label>
      </div>
      <div>
        <label>
          Manufacturer
          <select
            name="manufacturer_id"
            value={formData.manufacturer_id}
            onChange={handleChange}
            required
            className="form-select"
          >
            <option>Choose a Manufacturer</option>
            {manufacturers.map((manufacturer) => (
              <option key={manufacturer.id} value={manufacturer.id}>
                {manufacturer.name}
              </option>
            ))}
          </select>
        </label>
      </div>
      <div>
        <button type="submit">Create</button>
      </div>
    </form>
  );
}

export default CreateVehicle;
