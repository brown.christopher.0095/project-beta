import React, { useEffect, useState } from "react";

function ListManufacturer() {
  const [manufacturers, setManufacturer] = useState([]);

  const fetchData = async () => {
    const response = await fetch("http://localhost:8100/api/manufacturers/");
    try {
      if (response.ok) {
        const data = await response.json();
        setManufacturer(data.manufacturers);
      }
    } catch (e) {
      console.log("Error fetching manufacturers");
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="d-flex justify-content-center align-items-center">
      
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
          </tr>
        </thead>
        <tbody>
          {manufacturers.map((manufacturer) => {
            return (
              <tr key={manufacturer.id}>
                <td>{manufacturer.name}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ListManufacturer;
