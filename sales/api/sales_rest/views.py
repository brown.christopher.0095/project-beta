from django.forms import ValidationError
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.core.exceptions import ObjectDoesNotExist
import json
from .models import AutomobileVO, Customer, SalesPerson, Sale

# ModelEncoder base class
class ModelEncoder:
    def encode(self, obj):
        raise NotImplementedError("You must implement the encode method.")

class AutomobileVOEncoder(ModelEncoder):
    def encode(self, obj):
        return {
            "vin": obj.vin,
            "sold": obj.sold
        }

class CustomerEncoder(ModelEncoder):
    def encode(self, obj):
        return {
            "first_name": obj.first_name,
            "last_name": obj.last_name,
            "address": obj.address,
            "phone_number": obj.phone_number
        }

class SalesPersonEncoder(ModelEncoder):
    def encode(self, obj):
        return {
            "first_name": obj.first_name,
            "last_name": obj.last_name,
            "employee_id": obj.employee_id
        }

class SaleEncoder(ModelEncoder):
    def encode(self, obj):
        return {
            "price": str(obj.price),
            "customer": CustomerEncoder().encode(obj.customer),
            "sales_person": SalesPersonEncoder().encode(obj.salesperson),
            "automobile": AutomobileVOEncoder().encode(obj.automobile)
        }

# API Views
@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        encoded_customers = [CustomerEncoder().encode(customer) for customer in customers]
        return JsonResponse({"customers": encoded_customers}, safe=False)
    elif request.method == "POST":
        data = json.loads(request.body)
        customer, created = Customer.objects.get_or_create(**data)
        return JsonResponse({"customer": CustomerEncoder().encode(customer)}, safe=False, status=(201 if created else 200))

@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = SalesPerson.objects.all()
        encoded_salespeople = [SalesPersonEncoder().encode(salesperson) for salesperson in salespeople]
        return JsonResponse({"salespeople": encoded_salespeople}, safe=False)
    elif request.method == "POST":
        data = json.loads(request.body)
        salesperson, created = SalesPerson.objects.get_or_create(**data)
        return JsonResponse({"salesperson": SalesPersonEncoder().encode(salesperson)}, safe=False, status=(201 if created else 200))

@require_http_methods(["GET", "POST"])
def api_list_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        # Manually encode each sale using SaleEncoder
        sales_data = [SaleEncoder().encode(sale) for sale in sales]
        # Directly return the list of encoded sales
        return JsonResponse({"sales": sales_data}, safe=False)
    elif request.method == "POST":
        data = json.loads(request.body)
        try:
            automobile = AutomobileVO.objects.get(pk=data['automobile'])
            customer = Customer.objects.get(pk=data['customer'])
            salesperson = SalesPerson.objects.get(pk=data['salesperson'])
            
            sale = Sale(automobile=automobile, customer=customer, salesperson=salesperson, price=data['price'])
            try:
                sale.full_clean()  # Validates the model instance
                sale.save()
                automobile.sold = True
                automobile.save()
                return JsonResponse({"sale": SaleEncoder().encode(sale)}, safe=False, status=201)
            except ValidationError as e:
                return JsonResponse({"error": str(e)}, status=400)
        except ObjectDoesNotExist as e:
            return JsonResponse({"error": "One of the related instances does not exist."}, status=404)
        except Exception as e:
            return JsonResponse({"error": str(e)}, status=400)

@require_http_methods(["GET"])
def api_sales_by_salesperson(request, salesperson_id):
    try:
        sales = Sale.objects.filter(salesperson__id=salesperson_id)
        encoded_sales = [SaleEncoder().encode(sale) for sale in sales]
        return JsonResponse({"sales": encoded_sales}, safe=False)
    except ObjectDoesNotExist as e:
        return JsonResponse({"error": str(e)}, status=404)