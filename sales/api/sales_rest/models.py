from django.db import models

# Create your models here.

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

class SalesPerson(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.IntegerField(unique=True)

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name, self.employee_id)
    

class Customer(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=10)



class Sale(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    salesperson = models.ForeignKey(SalesPerson, on_delete=models.CASCADE)
    automobile = models.ForeignKey(AutomobileVO, on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=10, decimal_places=2)


    